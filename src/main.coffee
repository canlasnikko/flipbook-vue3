import Vue, { createApp } from 'vue'
import App from './App.vue'

Vue.configureCompat({ WATCH_ARRAY: false })
Vue.configureCompat({ RENDER_FUNCTION: false })

createApp(App).mount('#app')
