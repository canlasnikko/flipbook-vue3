module.exports = {
  publicPath: '',
  css: {
    extract: false
  },
  chainWebpack: config => {
    config.resolve.alias.set('vue', '@vue/compat')

    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        return {
          ...options,
          compilerOptions: {
            compatConfig: {
              MODE: 2
            }
          }
        }
      })

    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('url-loader')
        .loader('url-loader')
        .end()
      .use('svgo-loader')
        .loader('svgo-loader')
        .options({ plugins: [ { removeViewBox: false } ] })
        .end()
  }
}
